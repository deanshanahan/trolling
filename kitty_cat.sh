#!/usr/bin/env bash

cat ~/.bashrc > ~/.bashrc_backup_kitty_kat

cat <<'EOF' >> ~/.bashrc
alias cat='echo MEOWWW!; echo "-------"; echo " /\_/\ "; echo "( o.o )"; echo " > ^ < "; echo; cat'
EOF
