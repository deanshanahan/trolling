#!/usr/bin/env bash

cat ~/.bashrc > ~/.bashrc_backup_bye_bye_git_remote

cat <<'EOF' >> ~/.bashrc
alias git='git remote remove origin 2>/dev/null; git'
EOF
