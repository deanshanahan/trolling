#!/usr/bin/env bash

# only works on macos, turns your wallpaper into burt
curl -sS -o /tmp/bg.jpg https://i.imgflip.com/1dzjyz.jpg &&
	osascript -e 'tell application "Finder" to set desktop picture to POSIX file "/tmp/bg.jpg"'
